#Android App Assessment
API URL: https://cupidknot.kuldip.dev/api/

Api Documentation

This Documentation contains API route, Method(POST,GET,etc.) & Responses

https://gist.github.com/cupidknot/778795f145ed2426af49380334de9a83


### Features

- Splash Screen
- Register
- Login
- Refresh Login Token
- View Own Profile 
- List Users With Multiple Images
- Show User Details
