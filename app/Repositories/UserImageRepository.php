<?php

namespace App\Repositories;

use App\Models\UserImage;
use App\Repositories\BaseRepository;

/**
 * Class UserImageRepository
 * @package App\Repositories
 * @version November 10, 2020, 9:55 am UTC
*/

class UserImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'name',
        'path',
        'type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserImage::class;
    }
}
