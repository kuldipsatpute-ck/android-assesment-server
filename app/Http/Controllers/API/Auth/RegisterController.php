<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\AppBaseController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Client;
class RegisterController extends AppBaseController
{
    use IssueTokenTrait;

    private $client;

    public function __construct(){
        $this->client = Client::find(2);
    }

    public function register(Request $request){

        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8|confirmed',
            'birth_date' => 'date:Y-m-d',
            'gender' => 'required|string|in:MALE,FEMALE',
        ]);



        $user = User::create([
            'name' => request('first_name') ." " . request('last_name'),
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
            'birth_date' => $request->has('birth_date') ? request('birth_date') : null,
            'gender' =>  $request->has('gender') ? request('gender') : null,
        ]);

        return $this->issueToken($request, 'password');

    }
}
