<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\AppBaseController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Client;

class LoginController extends AppBaseController
{

    use IssueTokenTrait;

    private $client;

    public function __construct(){
        $this->client = Client::find(2);
    }

    public function login(Request $request){

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $user = User::query()->where('email', $request->post('username'))->get();
        if ($user->count() == 0){
            return $this->sendError("User not found", 404);
        }

        return $this->issueToken($request, 'password');

    }

    public function refresh(Request $request){
        $this->validate($request, [
            'refresh_token' => 'required'
        ]);

        return $this->issueToken($request, 'refresh_token');

    }

    public function logout(Request $request){

        $accessToken = Auth::user()->token();

        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update(['revoked' => true]);

        $accessToken->revoke();

        return response()->json(['message' => 'GoodBye!'], 200);

    }

    public function getUser(Request $request){
        $this->validate($request, [
            'username' => 'required'
        ]);



        $user = User::where('email',$request->username)
            ->select('id','name','email')
            ->get();

        if ($user == null ){
            return $this->sendError("Sorry! User Not Found!",404);
        } else {
            return $this->sendResponse($user[0],"Success");
        }
    }

    public function change_password(Request $request){

        if (!(Hash::check($request->input('current_password'), Auth::user()->password))) {
            // The passwords matches
            return $this->sendError("Your current password is incorrect!",401);
        }
        if(strcmp($request->input('current_password'), $request->input('new_password')) == 0){
            //Current password and new password are same
            return $this->sendError("New Password CANNOT BE THE SAME!",401);
        }
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:8|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($request->input('new_password'));
        $user->save();
        return $this->sendResponse("success","Password changed successfully !");
    }

    public function updateProfile($id,Request $request){
        $input = $request->all();

        /** @var \App\Models\User $user */
        $user = User::find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->update($input);

        return $this->sendResponse($user->toArray(), 'User updated successfully');
    }

    public function profile_pic(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'uploadFile' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('uploadFile')) {
            $uploadedFile = $request->file('uploadFile');
            $filename = time() . $uploadedFile->getClientOriginalName();
            $file = Storage::disk('local')->putFileAs(
                '/public/images/UserProfilePictures',
                $uploadedFile,
                $filename
            );

            $user->avatar = config('app.url') .'/storage/images/UserProfilePictures/'. $filename;//
            $user->save();

            return $this->sendResponse($user->avatar,"Profile Picture Updated Successfully!");
        }

    }

    public function forget_password(Request $request){

        return "";
    }
}
