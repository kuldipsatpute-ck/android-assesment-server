<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserImageAPIRequest;
use App\Http\Requests\API\UpdateUserImageAPIRequest;
use App\Models\UserImage;
use App\Repositories\UserImageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class UserImageController
 * @package App\Http\Controllers\API
 */

class UserImageAPIController extends AppBaseController
{
    /** @var  UserImageRepository */
    private $userImageRepository;

    public function __construct(UserImageRepository $userImageRepo)
    {
        $this->userImageRepository = $userImageRepo;
    }

    /**
     * Display a listing of the UserImage.
     * GET|HEAD /userImages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $userImages = $this->userImageRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($userImages->toArray(), 'User Images retrieved successfully');
    }

    /**
     * Store a newly created UserImage in storage.
     * POST /userImages
     *
     * @param CreateUserImageAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $userid = auth()->id();

        if ($request->hasFile('original_photo')) {
            $files = $request->file('original_photo');
            $destinationPath = public_path('/document_images');
            foreach ($files as $file) {
                $original_photo = time() . '_' . $file->getClientOriginalName();
                $file->move($destinationPath, $original_photo);

                $userdetailsdata['user_id'] = $userid;
                if ($file->getClientOriginalExtension() == "doc" || $file->getClientOriginalExtension() == "pdf") {
                    $userdetailsdata['type'] = 'original_document';
                } else {
                    $userdetailsdata['type'] = 'original_image';
                }
                $userdetailsdata['name'] = $original_photo;
                $userdetailsdata['path'] = config('app.url') . "/document_images/";
                $userimage = UserImage::create($userdetailsdata);
            }
        }

        return $this->sendResponse(["images"=>"Saved"], 'User Image saved successfully');

    }

    /**
     * Display the specified UserImage.
     * GET|HEAD /userImages/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var UserImage $userImage */
        $userImage = $this->userImageRepository->find($id);

        if (empty($userImage)) {
            return $this->sendError('User Image not found');
        }

        return $this->sendResponse($userImage->toArray(), 'User Image retrieved successfully');
    }

    /**
     * Update the specified UserImage in storage.
     * PUT/PATCH /userImages/{id}
     *
     * @param int $id
     * @param UpdateUserImageAPIRequest $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $input = $request->all();
        $id = auth()->id();

        /** @var UserImage $userImage */
        $userImage = $this->userImageRepository->find($id);

        if (empty($userImage)) {
            return $this->sendError('User Image not found');
        }

        $userImage = $this->userImageRepository->update($input, $id);

        return $this->sendResponse($userImage->toArray(), 'UserImage updated successfully');
    }

    /**
     * Remove the specified UserImage from storage.
     * DELETE /userImages/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var UserImage $userImage */
        $userImage = $this->userImageRepository->find($id);

        if (empty($userImage)) {
            return $this->sendError('User Image not found');
        }

        $userImage->delete();

        return $this->sendSuccess('User Image deleted successfully');
    }
}
