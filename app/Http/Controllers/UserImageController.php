<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserImageRequest;
use App\Http\Requests\UpdateUserImageRequest;
use App\Repositories\UserImageRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class UserImageController extends AppBaseController
{
    /** @var  UserImageRepository */
    private $userImageRepository;

    public function __construct(UserImageRepository $userImageRepo)
    {
        $this->userImageRepository = $userImageRepo;
    }

    /**
     * Display a listing of the UserImage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $userImages = $this->userImageRepository->all();

        return view('user_images.index')
            ->with('userImages', $userImages);
    }

    /**
     * Show the form for creating a new UserImage.
     *
     * @return Response
     */
    public function create()
    {
        return view('user_images.create');
    }

    /**
     * Store a newly created UserImage in storage.
     *
     * @param CreateUserImageRequest $request
     *
     * @return Response
     */
    public function store(CreateUserImageRequest $request)
    {
        $input = $request->all();

        $userImage = $this->userImageRepository->create($input);

        Flash::success('User Image saved successfully.');

        return redirect(route('userImages.index'));
    }

    /**
     * Display the specified UserImage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userImage = $this->userImageRepository->find($id);

        if (empty($userImage)) {
            Flash::error('User Image not found');

            return redirect(route('userImages.index'));
        }

        return view('user_images.show')->with('userImage', $userImage);
    }

    /**
     * Show the form for editing the specified UserImage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userImage = $this->userImageRepository->find($id);

        if (empty($userImage)) {
            Flash::error('User Image not found');

            return redirect(route('userImages.index'));
        }

        return view('user_images.edit')->with('userImage', $userImage);
    }

    /**
     * Update the specified UserImage in storage.
     *
     * @param int $id
     * @param UpdateUserImageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserImageRequest $request)
    {
        $userImage = $this->userImageRepository->find($id);

        if (empty($userImage)) {
            Flash::error('User Image not found');

            return redirect(route('userImages.index'));
        }

        $userImage = $this->userImageRepository->update($request->all(), $id);

        Flash::success('User Image updated successfully.');

        return redirect(route('userImages.index'));
    }

    /**
     * Remove the specified UserImage from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userImage = $this->userImageRepository->find($id);

        if (empty($userImage)) {
            Flash::error('User Image not found');

            return redirect(route('userImages.index'));
        }

        $this->userImageRepository->delete($id);

        Flash::success('User Image deleted successfully.');

        return redirect(route('userImages.index'));
    }
}
