<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('get_user','Auth\LoginController@getUser')->name('get_user');
Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('refresh', 'Auth\LoginController@refresh');
Route::post('forget-password', 'Auth\LoginController@forget_password');


Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');
});
Route::middleware('auth:api')->get('/user', function (Request $request) {

    return \App\Models\User::find(auth()->id());
});

Route::group(['middleware' => 'auth:api'], function (){
    Route::post('update_user', 'UserAPIController@update');
    Route::get('users', 'UserAPIController@index');
//    Route::resource('users', 'UserAPIController');
    Route::post('user_images', 'UserImageAPIController@store');

});


