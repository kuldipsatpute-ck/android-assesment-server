<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\UserImage;

class UserImageApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_user_image()
    {
        $userImage = factory(UserImage::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/user_images', $userImage
        );

        $this->assertApiResponse($userImage);
    }

    /**
     * @test
     */
    public function test_read_user_image()
    {
        $userImage = factory(UserImage::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/user_images/'.$userImage->id
        );

        $this->assertApiResponse($userImage->toArray());
    }

    /**
     * @test
     */
    public function test_update_user_image()
    {
        $userImage = factory(UserImage::class)->create();
        $editedUserImage = factory(UserImage::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/user_images/'.$userImage->id,
            $editedUserImage
        );

        $this->assertApiResponse($editedUserImage);
    }

    /**
     * @test
     */
    public function test_delete_user_image()
    {
        $userImage = factory(UserImage::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/user_images/'.$userImage->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/user_images/'.$userImage->id
        );

        $this->response->assertStatus(404);
    }
}
