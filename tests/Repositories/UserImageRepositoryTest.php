<?php namespace Tests\Repositories;

use App\Models\UserImage;
use App\Repositories\UserImageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class UserImageRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var UserImageRepository
     */
    protected $userImageRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->userImageRepo = \App::make(UserImageRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_user_image()
    {
        $userImage = factory(UserImage::class)->make()->toArray();

        $createdUserImage = $this->userImageRepo->create($userImage);

        $createdUserImage = $createdUserImage->toArray();
        $this->assertArrayHasKey('id', $createdUserImage);
        $this->assertNotNull($createdUserImage['id'], 'Created UserImage must have id specified');
        $this->assertNotNull(UserImage::find($createdUserImage['id']), 'UserImage with given id must be in DB');
        $this->assertModelData($userImage, $createdUserImage);
    }

    /**
     * @test read
     */
    public function test_read_user_image()
    {
        $userImage = factory(UserImage::class)->create();

        $dbUserImage = $this->userImageRepo->find($userImage->id);

        $dbUserImage = $dbUserImage->toArray();
        $this->assertModelData($userImage->toArray(), $dbUserImage);
    }

    /**
     * @test update
     */
    public function test_update_user_image()
    {
        $userImage = factory(UserImage::class)->create();
        $fakeUserImage = factory(UserImage::class)->make()->toArray();

        $updatedUserImage = $this->userImageRepo->update($fakeUserImage, $userImage->id);

        $this->assertModelData($fakeUserImage, $updatedUserImage->toArray());
        $dbUserImage = $this->userImageRepo->find($userImage->id);
        $this->assertModelData($fakeUserImage, $dbUserImage->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_user_image()
    {
        $userImage = factory(UserImage::class)->create();

        $resp = $this->userImageRepo->delete($userImage->id);

        $this->assertTrue($resp);
        $this->assertNull(UserImage::find($userImage->id), 'UserImage should not exist in DB');
    }
}
