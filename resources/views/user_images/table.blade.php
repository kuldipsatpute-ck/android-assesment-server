<div class="table-responsive">
    <table class="table" id="userImages-table">
        <thead>
            <tr>
                <th>User Id</th>
        <th>Name</th>
        <th>Path</th>
        <th>Type</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($userImages as $userImage)
            <tr>
                <td>{{ $userImage->user_id }}</td>
            <td>{{ $userImage->name }}</td>
            <td>{{ $userImage->path }}</td>
            <td>{{ $userImage->type }}</td>
                <td>
                    {!! Form::open(['route' => ['userImages.destroy', $userImage->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('userImages.show', [$userImage->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('userImages.edit', [$userImage->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
