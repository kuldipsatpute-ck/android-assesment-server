@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            User Image
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($userImage, ['route' => ['userImages.update', $userImage->id], 'method' => 'patch']) !!}

                        @include('user_images.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection