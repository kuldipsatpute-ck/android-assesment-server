#API Docs


### POST : /api/register 

Parameters
```
rules = {
        'first_name' => 'required|string|max:255',
        'last_name' => 'required|string|max:255'
        'email' => 'required|string|max:255',
        'email_verified_at' => 'nullable',
        'password' => 'required|string|max:255',
        'password_confirmation' => 'required|string|max:255',
}
```
---
### POST : /api/login 

Parameters
```
rules = {
        'username' => 'required|string|max:255',
        'password' => 'required|string|max:255',
}
```
Response: Success
```
{
    "token_type": "Bearer",
    "expires_in": 3600,
    "access_token": "ACCESS_TOKEN",
    "refresh_token": "REFRESH_TOKEN"
}
```

---
### POST : /api/refresh 
Parameters
```
rules = {
        'refresh_token' => 'required',
}
```
Response: Success
```
{
    "token_type": "Bearer",
    "expires_in": 3600,
    "access_token": "ACCESS_TOKEN",
    "refresh_token": "REFRESH_TOKEN"
}
```
Error Response: Refresh Token Expired
```
{
    "error": "invalid_request",
    "error_description": "The refresh token is invalid.",
    "hint": "Token has been revoked",
    "message": "The refresh token is invalid."
}
```
### GET : /api/user
HEADERS
```
{
    "Accept": "application/json",
    "Authorization": "Bearer ACCESS_TOKEN"
}
```
### GET : /api/users
HEADERS
```
{
    "Accept": "application/json",
    "Authorization": "Bearer ACCESS_TOKEN",
}
```
