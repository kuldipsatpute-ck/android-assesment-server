<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserImage;
use Faker\Generator as Faker;

$factory->define(UserImage::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'name' => $faker->word,
        'path' => $faker->word,
        'type' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
